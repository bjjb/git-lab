require "../gitlab"

class GitLab
  # The base class for GitLab resources fetched from the server.
  abstract class Resource < self
    include JSON::Serializable

    def to_s(io : IO)
      to_pretty_json(io)
    end
  end
end
