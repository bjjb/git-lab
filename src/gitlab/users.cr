require "./user"
require "./collection"

class GitLab
  class Users < Collection(User)
  end

  def users
    Users.from(get("users"))
  end
end
