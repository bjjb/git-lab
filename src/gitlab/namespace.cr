require "./resource"

class GitLab
  class Namespace < Resource
    getter id : UInt128
    getter name : String
    getter path : String
    getter kind : String
    getter full_path : String
    getter parent_id : UInt128?
    getter avatar_url : String?
    getter web_url : String?
    getter members_count_with_descendants : UInt128?
    getter billable_members_count : UInt128?
    getter max_seats_used : UInt128?
    getter seats_in_use : UInt128?
    getter plan : String?
    getter end_date : Time?
    getter trial_ends_on : Time?
    getter trial : Bool?
    getter root_repository_size : UInt128?
    getter projects_count : UInt128?
  end
end
