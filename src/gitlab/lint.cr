require "./resource"

class GitLab
  class Lint < Resource
    getter valid : Bool
    getter merged_yaml : String
    getter errors : Array(String)
    getter warnings : Array(String)
  end
end
