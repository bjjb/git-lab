require "./resource"

class GitLab
  class Markdown < Resource
    getter html : String
  end

  def markdown(text : String, gfm : Bool = true, project : (UInt128 | String)? = nil)
    params = {text: text, gfm: gfm, project: project.try(&.to_s)}
    Markdown.from_json(post("markdown", params).body)
  end
end
