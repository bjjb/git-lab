require "./resource"

class GitLab
  class Version < Resource
    getter version : String
    getter revision : String
    getter enterprise : Bool
  end

  def version
    Version.from_json(get("version").body)
  end
end
