class GitLab
  module Environment
    macro method_missing(key)
      {% if key.id.ends_with?('?') %}
	def {{ key.id }}
	  !{{ key.id[0..-2] }}.presence.nil?
	end
      {% elsif key.id.ends_with?('!') %}
	def {{ key.id }}
	  ENV.fetch("{{ key.id.[0..-2].upcase }}")
        end
      {% else %}
	def {{ key.id }}
	  ENV["{{ key.id.upcase }}"]?
	end
      {% end %}
    end
  end
end
