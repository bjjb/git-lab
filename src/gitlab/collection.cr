class GitLab
  class Collection(T) < self
    @current : Array(T)
    @next : URI?

    include Iterator(T)

    def self.next(links : String?)
      links.split(/\s*,\s*/).find(&.ends_with?(/;\s*rel="next"/)).try do |link|
        URI.parse(link.split(/\s*;\s*/).first.lstrip('<').rstrip('>'))
      end if links
    end

    def self.from(resp)
      new(Array(T).from_json(resp.body), self.next(resp.headers["Link"]?))
    end

    def initialize(@current : Array(T), @next : URI?)
    end

    def next
      next! if @current.empty?
      @current.shift? || stop
    end

    private def next!
      @next.try do |uri|
        get(uri).tap do |resp|
          @current = Array(T).from_json(resp.body)
          @next = self.class.next(resp.headers["Link"])
        end
      end
    end
  end
end
