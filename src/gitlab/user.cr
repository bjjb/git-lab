require "./resource"
require "./projects"

class GitLab
  class User < Resource
    getter id : UInt128
    getter username : String
    getter state : String
    getter locked : Bool
    getter avatar_url : String
    getter web_url : String
    getter created_at : Time?
    getter bio : String?
    getter bot : Bool?
    getter location : String?
    getter public_email : String?
    getter skype : String?
    getter linkedin : String?
    getter twitter : String?
    getter discord : String?
    getter website_url : String?
    getter organization : String?
    getter job_title : String?
    getter pronouns : String?
    getter work_information : String?
    getter followers : UInt32?
    getter following : UInt32?
    getter local_time : String?
    getter is_followed : Bool?

    def projects
      Projects.from(get("users/#{id}/projects"))
    end
  end

  class Me < User
    def status
      Status.from_json(get("user/status").body)
    end

    def preferences
      Preferences.from_json(get("user/preferences").body)
    end
  end

  class Status < Resource
    getter emoji : String
    getter availability : String
    getter message : String
    getter message_html : String
    getter clear_status_at : Time?
  end

  class Preferences < Resource
    getter id : UInt128
    getter user_id : UInt128
    getter view_diffs_file_by_file : Bool
    getter show_whitespace_in_diffs : Bool
    getter pass_user_identities_to_ci_jwt : Bool
  end

  def user : Me
    Me.from_json(get("user").body)
  end

  def user(id) : User
    User.from_json(get("users/#{id}").body)
  end
end
