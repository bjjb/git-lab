require "./collection"
require "./project"

class GitLab
  class Projects < Collection(Project)
  end
end
