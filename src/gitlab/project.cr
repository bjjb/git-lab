require "./resource"
require "./lint"

class GitLab
  class Project < Resource
    record Owner,
      id : UInt128,
      name : String?,
      created_at : Time? do
      include JSON::Serializable
    end
    record Namespace,
      id : UInt128,
      name : String?,
      path : String?,
      kind : String?,
      full_path : String? do
      include JSON::Serializable
    end
    record Statistics,
      commit_count : UInt32?,
      storage_size : UInt32?,
      repository_size : UInt32?,
      wiki_size : UInt32?,
      lfs_objects_size : UInt32?,
      job_artifacts_size : UInt32?,
      pipeline_artifacts_size : UInt32?,
      packages_size : UInt32?,
      snippets_size : UInt32?,
      uploads_size : UInt32?,
      container_registry_size : UInt32? do
      include JSON::Serializable
    end
    record Links,
      self : String?,
      issues : String?,
      merge_requests : String?,
      repo_branches : String?,
      labels : String?,
      events : String?,
      members : String?,
      cluster_agents : String? do
      include JSON::Serializable
    end
    getter id : UInt128?
    getter description : String?
    getter description_html : String?
    getter default_branch : String?
    getter visibility : String?
    getter ssh_url_to_repo : String?
    getter http_url_to_repo : String?
    getter web_url : String?
    getter readme_url : String?
    getter topics : Array(String)?
    getter owner : Owner?
    getter name : String?
    getter name_with_namespace : String?
    getter path : String?
    getter path_with_namespace : String?
    getter issues_enabled : Bool?
    getter open_issues_count : Int32?
    getter merge_requests_enabled : Bool?
    getter jobs_enabled : Bool?
    getter wiki_enabled : Bool?
    getter snippets_enabled : Bool?
    getter can_create_merge_request_in : Bool?
    getter resolve_outdated_diff_discussions : Bool?
    getter container_registry_enabled : Bool?
    getter container_registry_access_level : String?
    getter security_and_compliance_access_level : String?
    getter created_at : Time?
    getter updated_at : Time?
    getter last_activity_at : Time?
    getter creator_id : UInt128?
    getter import_url : String?
    getter import_type : String?
    getter import_status : String?
    getter import_error : String?
    getter namespace : Namespace?
    getter import_status : String?
    getter archived : Bool?
    getter avatar_url : String?
    getter shared_runners_enabled : Bool?
    getter group_runners_enabled : Bool?
    getter forks_count : UInt32?
    getter star_count : UInt32?
    getter runners_token : String?
    getter ci_default_git_depth : UInt32?
    getter ci_forward_deployment_enabled : Bool?
    getter ci_forward_deployment_rollback_allowed : Bool?
    getter ci_allow_fork_pipelines_to_run_in_parent_project : Bool?
    getter ci_separated_caches : Bool?
    getter ci_restrict_pipeline_cancellation_role : String?
    getter ci_pipeline_variables_minimum_override_role : String?
    getter ci_push_repository_for_job_token_allowed : Bool?
    getter public_jobs : Bool?
    getter shared_with_groups : Array(String)?
    getter only_allow_merge_if_pipeline_succeeds : Bool?
    getter allow_merge_on_skipped_pipeline : Bool?
    getter restrict_user_defined_variables : Bool?
    getter only_allow_merge_if_all_discussions_are_resolved : Bool?
    getter remove_source_branch_after_merge : Bool?
    getter request_access_enabled : Bool?
    getter merge_method : String?
    getter squash_option : String?
    getter autoclose_referenced_issues : Bool?
    getter enforce_auth_checks_on_uploads : Bool?
    getter suggestion_commit_message : String?
    getter merge_commit_template : String?
    getter squash_commit_template : String?
    getter issue_branch_template : String?
    getter marked_for_deletion_on : Time?
    getter statistics : Statistics?
    getter container_registry_image_prefix : String?
    getter _links : Links?

    def lint
      content = File.read(".gitlab-ci.yml")
      Lint.from_json(post("projects/#{id}/ci/lint", {content: content}).body)
    end
  end
end
