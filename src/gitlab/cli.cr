require "option_parser"

class GitLab
  def self.cli(op : OptionParser)
    stdin = STDIN
    stdout = STDOUT
    stderr = STDERR

    gitlab = GitLab.new

    op.banner = "Usage: git-lab [options] [command [args...]]"

    op.unknown_args do |args, cmd|
      "Unknown argument(s): #{args.join(' ')}\n\n#{op}".to_s(stderr)
      exit 1
    end

    op.on "-h", "--help", "print help and exit" do
      op.unknown_args do
        op.to_s(stdout)
        exit
      end
    end

    op.on "-V", "--version", "print version information and exit" do
      op.unknown_args do
        "git-lab v#{GitLab::VERSION}".to_s(stdout)
        exit
      end
    end

    op.on "-t", "--token TOKEN", "specify a token" do |token|
      gitlab.token = token
    end

    op.on "-E", "--api-endpoint URL", "specify an API endpoint URL" do |endpoint|
      gitlab.api = URI.parse(endpoint)
    end

    op.on "get", "gets an arbitrary resource or resources" do
      op.banner = "Usage: git-lab get PATH..."
      op.unknown_args do |args, cmd|
        ok = true
        args.each do |arg|
          resp = gitlab.get(arg)
          next puts(resp.body) if resp.success?
          stderr.puts("Failed to fetch #{arg} (#{resp.status.code})")
          ok = false
        end
        exit ok ? 0 : 1
      end
    end

    op.on "endpoint", "print the GitLab API endpoint and exit" do
      op.unknown_args do
        gitlab.api.to_s(stdout)
        exit
      end
    end

    op.on "home", "print the GitLab local home directory" do
      op.unknown_args do
        gitlab.home.to_s(stdout)
        exit
      end
    end

    op.on "project", "print the current GitLab project" do
      op.banner = "git-lab project [ID]"
      op.separator
      op.separator %(
Prints the ID, path, working directory and web URL of the given project. If no
ID is provided, '.' is assumed. '.' corresponds to the current project, as
determined by checking the '.git' directory. If no project can be identified,
it will exit non-zero.)
      op.unknown_args do |args, cmd|
        p = args.shift || '.'
      end
    end

    op.on "token", "print the GitLab token and exit" do
      op.unknown_args do
        gitlab.token.to_s(stdout)
        exit
      end
    end

    op.on "version", "print the GitLab server version and exit" do
      op.unknown_args do
        gitlab.version.to_s(stdout)
        exit
      end
    end

    op.on "markdown", "convert markdown on stdin to HTML on stdout" do
      op.banner = "Usage: git-lab markdown"
      op.unknown_args do |args, cmd|
        puts(gitlab.markdown(text: stdin.gets_to_end).html)
      end
    end

    op.on "lint", "validate the content of the .gitlab-ci" do
      op.banner = "Usage: git-lab lint"
      op.unknown_args do |args|
        raise NotImplementedError.new("git-lab lint")
      end
    end

    op.on "user", "gets information about a user (or the calling user)" do
      op.on("status", "get the current user's status") do
        op.unknown_args do |args, cmd|
          gitlab.user.status.to_s(stdout)
        end
      end
      op.on("preferences", "get the current user's preferences") do
        op.unknown_args do |args, cmd|
          gitlab.user.preferences.to_s(stdout)
        end
      end
      op.on("associations-count", "get the specified user's counts") do
        op.unknown_args do |args, cmd|
          raise NotImplementedError.new("user associations-count")
        end
      end
      op.on("ssh-keys", "list a user's SSH keys") do
        op.unknown_args do |args, cmd|
          raise NotImplementedError.new("user ssh-keys")
        end
      end
      op.on("pgp-keys", "list a user's PGP keys") do
        op.unknown_args do |args, cmd|
          raise NotImplementedError.new("user pgp-keys")
        end
      end
      op.on("emails", "list a user's emails") do
        op.unknown_args do |args, cmd|
          raise NotImplementedError.new("user emails")
        end
      end
      op.on("impersonation-tokens", "lists a user's impersonation tokens") do
        op.on("-s", "--state", "filter tokens based on state") { }
        op.unknown_args do |args, cmd|
          raise NotImplementedError.new("user impersonation-tokens")
        end
      end
      op.on("activities", "list user activities") do
        op.unknown_args do |args, cmd|
          raise NotImplementedError.new("user activities")
        end
      end
      op.on("projects", "list the user's projects") do
        op.unknown_args do |args, cmd|
          gitlab.user.projects.each do |p|
            puts("##{p.id} <#{p.web_url}> #{p.description} #{p.topics.try { |t| "[#{t.join(',')}]" }}")
          end
        end
      end
      op.unknown_args do |args, cmd|
        gitlab.user.to_s(stdout) if args.empty?
        args.each { |id| puts gitlab.user(id) }
      end
    end

    op.on "users", "gets a list of users" do
      op.on("-u", "--username", "get a single user with the given username") { }
      op.on("-q", "--search", "search for a username") { }
      op.on("-a", "--active", "only active users") { }
      op.on("-x", "--external", "only external users") { }
      op.on("-X", "--no-external", "only non-external users") { }
      op.on("-I", "--no-internal", "only non-internal users") { }
      op.on("-B", "--no-bots", "only non-bot users") { }
      op.on("-s", "--created-after TIME", "filter for users created since TIME") { |t| }
      op.on("-b", "--created-before TIME", "filter for users created before TIME") { |t| }
      op.unknown_args { gitlab.users.each(&.to_s(stdout)) }
    end
  end
end
