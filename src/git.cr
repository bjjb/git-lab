require "semantic_version"

# A simple Git library based on libgit2.
class Git
  class Error < Exception
  end

  def initialize
    C.git_libgit2_init.tap { |n| raise Error.new("init failed: #{n}") if n.negative? }
  end

  getter version : SemanticVersion do
    C.git_libgit2_version(out major, out minor, out patch)
    prerelease = C.git_libgit2_prerelease.try { |p| String.new(p) unless p.null? }
    SemanticVersion.new(major, minor, patch, prerelease)
  end

  getter features : Features { C.features }

  def finalize
    C.git_libgit2_shutdown
  end

  @[Link("git2")]
  lib C
    fun git_libgit2_init : LibC::Int
    fun git_libgit2_shutdown : LibC::Int
    fun git_libgit2_features : Features
    fun git_libgit2_version(LibC::Int*, LibC::Int*, LibC::Int*) : LibC::Int
    fun git_libgit2_prerelease : LibC::Char*
    fun git_repository_open(Repository**, LibC::Char*) : LibC::Int
    fun git_repository_free(Repository*) : LibC::Int
    fun git_repository_path(Repository*) : LibC::Char*
    fun git_repository_commondir(Repository*) : LibC::Char*
    fun git_repository_config(Config**, Repository*) : LibC::Int
    fun git_config_free(Config*) : LibC::Int
    fun git_config_new(Config**) : LibC::Int
    fun git_config_iterator_new(ConfigIterator**, Config*) : LibC::Int
    fun git_config_next(ConfigEntry**, ConfigIterator*) : Errno
    fun git_config_iterator_free(ConfigIterator*) : LibC::Int
    fun git_config_entry_free(ConfigEntry*) : LibC::Int
    type Repository = Void*
    type Config = Void*
    type ConfigIterator = Void*

    struct ConfigEntry
      name : LibC::Char*
      value : LibC::Char*
      include_depth : LibC::Int
      level : ConfigLevel
      free : Void*
      payload : Void*
    end
  end

  @[Flags]
  enum Features
    Threads
    HTTPS
    SSH
    NSEC
  end

  @[Flags]
  enum Errno
    OK
    Error                =  -1
    NotFound             =  -3
    Exists               =  -4
    Ambiguous            =  -5
    OutputBufferTooShort =  -6
    User                 =  -7
    BareRepo             =  -8
    UnbornBranch         =  -9
    Unmerged             = -10
    CannotFastForward    = -11
    InvalidRefSpec       = -12
    Conflict             = -13
    Locked               = -14
    Modified             = -15
    AuthError            = -16
    InvalidCertificate   = -17
    AlreadyApplied       = -18
    PeelImpossible       = -19
    UnexpectedEOF        = -20
    Invalid              = -21
    Uncommitted          = -22
    Directory            = -23
    MergeConflict        = -24
    Passthrough          = -30
    IterationFinished    = -31
    Retry                = -32
    HashsumMismatch      = -33
    IndexDirty           = -34
    PatchFailed          = -35
    NotOwned             = -36
    Timeout              = -37
  end

  enum ConfigLevel
    ProgramData
    System
    XDG
    Global
    Local
    App
    Highest
  end

  private class_getter instance : self { new }
  class_getter version : SemanticVersion { instance.version }
  class_getter features : Features { instance.features }

  def repository(path : String | Path = ".")
    Repository.new(path)
  end

  class Repository
    @r : C::Repository*

    def initialize(path : String | Path = ".")
      C.git_repository_open(out @r, path)
    end

    private macro c_str(x)
 String.new(C.git_repository_{{x}}(@r)); end

    def path
      Path[c_str(path)].expand(home: true)
    end

    def commondir
      Path[c_str(commondir)].expand(home: true)
    end

    def config
      C.git_repository_config(out config, @r)
      Config.new(config)
    end

    def finalize
      C.git_repository_free(@r)
    end
  end

  class Config
    @c : C::Config*

    include Iterable({String, String})

    def initialize(@c)
    end

    def initialize
      C.git_config_new(out @c)
    end

    def each
      Iterator.new(@c)
    end

    def finalize
      C.git_config_free(@c)
    end

    class Entry
      @e : C::ConfigEntry*

      def initialize(@e)
      end

      def finalize
        C.git_config_entry_free(@e)
      end

      getter name : String { String.new(@e.name) }
    end

    class Iterator
      @i : C::ConfigIterator*

      include ::Iterator({String, String})

      def initialize(c)
        C.git_config_iterator_new(out @i, c)
      end

      def next
        return stop if C.git_config_next(out e, @i).iteration_finished?
        {String.new(e.value.name), String.new(e.value.value)}
      end

      def finalize
        C.git_config_iterator_free(@i)
      end
    end
  end
end
