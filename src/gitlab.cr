require "http/client"
require "json"

# A GitLab API client with some bells and whistles. It detects whether it's
# running in CI using the `CI` environment variable, and honours configuration
# from the `lab.*` git config sections.
class GitLab
  VERSION = "0.0.1"

  # Gets the command to run git-config(1)
  private def cfg
    ENV["GIT_EXEC_PATH"]?.try do |p|
      Path[p].expand(home: true).join("git-config")
    end || "git config"
  end

  # Gets the data configured (in git-config(1)) at the given 'lab' section.
  # Example:
  #
  # ```
  # system("git config lab.foo hello")
  # cfg("foo") # => "hello"
  # ```
  private def cfg(section : String)
    `#{cfg} lab.#{section}`.chomp.presence
  end

  # Yields if $CI is set.
  private def ci(&)
    yield if ENV["CI"]?
  end

  # Returns a CI variable with the given name (which will be upcased) if found.
  private def ci(var : String)
    ENV["CI_#{var.upcase}"]?
  end

  # The bearer token used to authorize API calls. It defaults to the first
  # value found in
  #
  # - 1. `$GITLAB_TOKEN`
  # - 2. (only if `$CI` is truthy) `$CI_DEPLOY_TOKEN` or `$CI_JOB_TOKEN`
  # - 3. the value of git-config(1) `lab.token`
  # - 4. the output of executing the value of git-config(1) `lab.tokenCommand`
  @[JSON::Field(ignore: true)]
  property token : String do
    ENV.fetch("GITLAB_TOKEN") do
      ci { ci("deploy_token") || ci("job_token") } ||
        cfg("token") || `#{cfg("tokenCommand")}`.chomp.presence ||
        raise NoToken
    end
  end

  # The REST API (v4) endpoint URI. Defaults to checking the `lab.api` git
  # config value, or falls back to https://gitlab.com/api/v4. In CI, it will
  # try the CI_API_V4_URL
  @[JSON::Field(ignore: true)]
  property api : URI do
    URI.parse(ci { ENV["CI_API_V4_URL"]? } || cfg("api") || "https://gitlab.com/api/v4")
  end

  # The local directory under which to store working directories, in a
  # hierarchy which mirrors groups, subgroups and projects. Defaults to
  @[JSON::Field(ignore: true)]
  property home : Path do
    # TODO: use libgit2
    Path[`git config lab.home`.chomp.presence || "~"].expand(home: true)
  end

  # A HTTP client for the API endpoint. It `Accept`s `application/json` and
  # sets the `Authorization` header to the bearer token.
  @[JSON::Field(ignore: true)]
  private property http : HTTP::Client do
    HTTP::Client.new(api).tap do |client|
      client.before_request do |request|
        request.headers["Accept"] ||= "application/json"
        request.headers["Authorization"] ||= "Bearer #{token}"
      end
    end
  end

  # Gets the given resource (prepending the API path if needed) and yields the
  # response to the block if it was successful. Non successful responses are
  # ignored.
  def get(path : String, & : HTTP::Client::Response ->)
    get(path).tap { |resp| yield(resp) if resp.status.success? }
  end

  # Gets the given resource. Prepends the API path if needed.
  def get(path : String)
    path = [api.path, path].join('/') unless path.starts_with?('/')
    http.get(path)
  end

  # Returns the response to a GET request to the given URI.
  def get(uri : URI)
    http.get(uri.to_s)
  end

  def post(path : String, data : NamedTuple)
    path = [api.path, path].join('/') unless path.starts_with?('/')
    headers = HTTP::Headers.new
    headers.add("Content-Type", "application/json")
    resp = http.post(path, headers, data.to_json)
    raise "[#{api}] POST #{path}: #{resp.status}" unless resp.success?
    return resp
  end

  class Error < Exception
  end

  # The error raised if the token is unset and cannot be determined.
  NoToken = Error.new("no GITLAB_TOKEN, set one with `git config lab.token`")

  def project
    # TODO: use libgit2
    Project.from_json(get("projects/#{URI.encode_path_segment(`git config remote.origin.url`.sub(%r{(?:https?://gitlab.com/|git@gitlab.com:)}, nil))}").body)
  end

  def project?
    # TODO - use libgit2.
    !`git config remote.origin.url`.chomp.empty?
  end

  def namespace
    d = Path[Dir.current]
    d = d.parent if project?
    d = d.relative_to(home)
    n = URI.encode_path_segment(d.to_s)
    Namespace.from_json(get("namespaces/#{n}").body)
  end
end

require "./gitlab/version"
require "./gitlab/user"
require "./gitlab/namespace"
require "./gitlab/users"
require "./gitlab/markdown"
