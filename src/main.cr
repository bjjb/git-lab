require "option_parser"
require "colorize"
require "./gitlab"

class OptionParser
  def assert_empty(*args : Array(String))
    return if args.all?(&.empty?)
    STDERR.puts("Unexpected argument(s): #{args}")
    exit 1
  end

  def run(&block : ->)
    unknown_args do |a, b|
      assert_empty(a, b)
      block.call
      exit 0
    end
  end

  def run(&block : (Array(String)) ->)
    unknown_args do |a, b|
      assert_empty(b)
      block.call(a)
      exit 0
    end
  end
end

OptionParser.parse do |op|
  gitlab = GitLab.new

  op.banner = "Usage: git-lab [options...] [command [args...]]"

  op.separator "Options:"

  op.on("-h", "--help", "print help and exit") { op.run { puts op } }

  op.separator "Commands:"

  op.on("token", "print the GitLab token") { op.run { puts gitlab.token } }
  op.on("home", "print the GitLab root dir") { op.run { puts gitlab.home } }

  op.on("get", "get arbitrary REST resources") do
    op.run { |args| args.each { |arg| puts gitlab.get(arg).body } }
  end

  op.on "id", "print the ID of the current project or namespace" do
    op.run do
      puts(gitlab.project? ? gitlab.project.id : puts gitlab.namespace.id)
    end
  end

  op.on "project?", "non-zero if the current directory isn't a project" do
    op.run { exit(1) unless gitlab.project? }
  end

  op.on "projects", "lists the projects in the current namespace" do
    op.run do
      if gitlab.project?
        gitlab.get("#{gitlab.project.id}/projects").body
      end
    end
  end

  op.on "whereami", "print the path to the current project or namespace" do
    op.run do
      puts "projects/#{gitlab.project.id}" if gitlab.project?
      puts "groups/#{gitlab.namespace.id}" unless gitlab.project?
    end
  end

  op.on "namespace", "TODO" do
    op.run do
      puts gitlab.namespace
    end
  end

  op.on "markdown", "convert GLFM to HTML" do
    p = gitlab.project.path_with_namespace if gitlab.project?
    op.run do
      puts gitlab.markdown(STDIN.gets_to_end, project: p).html
    end
  end

  op.on "lint", "lint a file" do
    q = false
    op.on("-q", "--quiet", "don't print the result, just exit 0 on success") do
      q = true
    end
    op.run do
      result = gitlab.project.lint
      exit(1) unless result.valid
      puts result.merged_yaml unless q
      result.warnings.try { |warnings| warnings.join("\n").to_s(STDERR) unless q }
      result.errors.try { |errors| errors.join("\n").to_s(STDERR) unless q }
      puts result.merged_yaml unless q
    end
  end

  op.unknown_args do |args, cmd|
    STDERR.puts op
    exit 1
  end
end
