require "spec"
require "../src/gitlab"

describe GitLab do
  pending "handles .gitignore (templates)" { }
  pending "handles .gitlab-ci.yml (templates)" { }
  pending "handles Access requests" { }
  pending "handles Appearance (application)" { }
  pending "handles Applications" { }
  pending "handles Audit events" { }
  pending "handles Avatar" { }
  pending "handles Award emoji" { }
  pending "handles Badges (project)" { }
  pending "handles Badges (group)" { }
  pending "handles Branches" { }
  pending "handles Broadcast messages" { }
  pending "handles Clusters (project)" { }
  pending "handles Clusters (group)" { }
  pending "handles Clusters (instance)" { }
  pending "handles Commits" { }
  pending "handles Composer" { }
  pending "handles Conan" { }
  pending "handles Container Registry" { }
  pending "handles Custom attributes" { }
  pending "handles Dashboard annotations" { }
  pending "handles Debian" { }
  pending "handles Debian group distributions" { }
  pending "handles Debian project distributions" { }
  pending "handles Dependencies" { }
  pending "handles Dependency Proxy" { }
  pending "handles Deploy keys" { }
  pending "handles Deploy tokens" { }
  pending "handles Deployments" { }
  pending "handles Discussions" { }
  pending "handles Dockerfile (templates)" { }
  pending "handles DORA4 metrics" { }
  pending "handles DORA4 project analytics" { }
  pending "handles Environments" { }
  pending "handles Epics" { }
  pending "handles Error tracking" { }
  pending "handles Events" { }
  pending "handles Experiments" { }
  pending "handles External status checks" { }
  pending "handles Features flags" { }
  pending "handles Feature flag user lists" { }
  pending "handles Freeze periods" { }
  pending "handles Geo nodes" { }
  pending "handles GitLab Pages" { }
  pending "handles Go Proxy" { }
  pending "handles Group activity analytics" { }
  pending "handles Group Import/Export" { }
  pending "handles Group relations export" { }
  pending "handles Group repository storage moves" { }
  pending "handles Group wikis" { }
  pending "handles Groups" { }
  pending "handles Helm" { }
  pending "handles Import" { }
  pending "handles Instance-level CI/CD variables" { }
  pending "handles Integrations" { }
  pending "handles Invitations" { }
  pending "handles Issue boards (project)" { }
  pending "handles Issue boards (group)" { }
  pending "handles Issues" { }
  pending "handles Issues (epic)" { }
  pending "handles Issues statistics" { }
  pending "handles Iterations (project)" { }
  pending "handles Iterations (group)" { }
  pending "handles Jobs" { }
  pending "handles Job artifacts" { }
  pending "handles Keys" { }
  pending "handles Labels (project)" { }
  pending "handles Labels (group)" { }
  pending "handles License" { }
  pending "handles Licenses (templates)" { }
  pending "handles Links (issue)" { }
  pending "handles Links (epic)" { }
  pending "handles Managed licenses" { }
  pending "handles Markdown" { }
  pending "handles Maven" { }
  pending "handles Members" { }
  pending "handles Merge request approvals" { }
  pending "handles Merge request context commits" { }
  pending "handles Merge requests" { }
  pending "handles Merge trains" { }
  pending "handles Migrations (bulk imports)" { }
  pending "handles Milestones (project)" { }
  pending "handles Milestones (group)" { }
  pending "handles Namespaces" { }
  pending "handles Notes (comments)" { }
  pending "handles Notification settings" { }
  pending "handles npm" { }
  pending "handles NuGet" { }
  pending "handles Packages" { }
  pending "handles Pages domains" { }
  pending "handles Personal access tokens" { }
  pending "handles Pipelines schedules" { }
  pending "handles Pipeline triggers" { }
  pending "handles Pipelines" { }
  pending "handles Plan limits" { }
  pending "handles Project access tokens" { }
  pending "handles Project aliases" { }
  pending "handles Project import/export" { }
  pending "handles Project remote mirrors" { }
  pending "handles Project repository storage moves" { }
  pending "handles Project statistics" { }
  pending "handles Project templates" { }
  pending "handles Project vulnerabilities" { }
  pending "handles Projects" { }
  pending "handles Protected branches" { }
  pending "handles Project-level protected environments" { }
  pending "handles Group-level protected environments" { }
  pending "handles Protected tags" { }
  pending "handles PyPI" { }
  pending "handles Releases" { }
  pending "handles Release links" { }
  pending "handles Repositories" { }
  pending "handles Repository files" { }
  pending "handles Repository submodules" { }
  pending "handles Resource iteration events" { }
  pending "handles Resource label events" { }
  pending "handles Resource milestone events" { }
  pending "handles Resource state events" { }
  pending "handles Resource weight events" { }
  pending "handles Ruby gems" { }
  pending "handles Runners" { }
  pending "handles SCIM" { }
  pending "handles Search" { }
  pending "handles Services" { }
  pending "handles Settings (application)" { }
  pending "handles Sidekiq metrics" { }
  pending "handles Sidekiq queues" { }
  pending "handles Snippet repository storage moves" { }
  pending "handles Snippets" { }
  pending "handles Snippets (project)" { }
  pending "handles Statistics (application)" { }
  pending "handles Suggestions" { }
  pending "handles System hooks" { }
  pending "handles Tags" { }
  pending "handles To-Do lists" { }
  pending "handles Service Data" { }
  pending "handles Users" { }
  pending "handles User-starred metrics dashboards" { }
  pending "handles Variables (project)" { }
  pending "handles Variables (group)" { }
  pending "handles Version" { }
  pending "handles Visual Review discussions" { }
  pending "handles Vulnerabilities" { }
  pending "handles Vulnerability export" { }
  pending "handles Vulnerability Findings" { }
  pending "handles Wikis" { }
end
