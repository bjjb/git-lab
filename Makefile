.PHONY: test lint test docs site

exe = bin/git-lab
src = src/*.cr src/gitlab/*.cr

$(exe): $(src)
	@shards build --error-on-warnings --release

format:
	@crystal tool format

lint:
	@crystal tool format --check

test:
	@crystal spec --tag ~slow

test/all:
	@crystal spec --tag

docs:
	@crystal docs

site:
	@crystal run src/main.cr -- pages build --output public
