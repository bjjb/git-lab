# git-lab

[git](https://git-scm.com) + [lab](https://gitlab.com) = 💰!

This little executable (if on your `$PATH`) adds another command to git:
`lab`. Its default action is to print out some information about its own
configuration; its subcommands provide some wrappers around calls to the
[GitLab API](https://docs.gitlab.com/ee/api/).

It's also a library that lets you build your own GitLab-related tools, and can
run a basic server.

The behaviour is slightly enhanced if in a git work-dir whose origin is a
GitLab project.

## Installation

**Write me**

## Usage

The tool is self-documenting, so just install it and run `git lab help`.
A cheatsheet is available on the [website](https://bjjb.gitlab.io/git-lab).

The documentation for the library is [hosted
here](https://crystalshards.org/shards/gitlab/bjjb/git-lab).

## Development

After checking out the repo, run `shards install` to install dependencies. Then,
run `crystal spec` to run the tests.

You can build the binary with `shards build`; the resultant `git-lab` binary
will be in `bin/`.

### Notes

* On MacOS, you may encounter an error like this:
  > ld: library not found for -lssl (this usually means you need to install
  > the development package for libssl)
  This informs you that the libssl library could not be found, which may
  indeed be the case if you've installed `libressl` using homebrew. (The
  library is required to communicate with the GitLab API over HTTPS). You can
  fix this by having the shell provide a hint to the linker about where
  additional packages can be found as follows:

    export PKG_CONFIG_PATH=/usr/local/opt/openssl/lib/pkgconfig

## Contributing

Bug reports and pull requests are welcome on
[GitLab](https://gitlab.com/bjjb/git-lab). This project is intended to be a
safe, welcoming space for collaboration, and contributors are expected to
adhere to the [guidelines](./CONTRIBUTING.md).

## License

All code is available as open source under the terms of the [MIT
License](https://opensource.org/licenses/MIT).
